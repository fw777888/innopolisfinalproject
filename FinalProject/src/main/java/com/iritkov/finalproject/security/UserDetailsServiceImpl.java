package com.iritkov.finalproject.security;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.iritkov.finalproject.entity.Account;
import com.iritkov.finalproject.form.AutorizedUser;
import com.iritkov.finalproject.repository.AccountRepository;

@RequiredArgsConstructor
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

   private final AccountRepository accountRepository;
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Account user = accountRepository.findByEmail(email)
                    .orElseThrow(()->new UsernameNotFoundException("Не найден пользователем с таким логином"));
        AutorizedUser.role = user.getRole();
        AutorizedUser.id = user.getId();
        AutorizedUser.name = user.getName();

        UserDetailsImpl userDetails = new UserDetailsImpl(user);
        return userDetails;
    }
}

package com.iritkov.finalproject.controller;

import com.iritkov.finalproject.entity.Role;
import com.iritkov.finalproject.entity.Song;
import com.iritkov.finalproject.form.AutorizedUser;
import com.iritkov.finalproject.form.SongForm;
import com.iritkov.finalproject.service.SongService;
import com.iritkov.finalproject.exception.SongNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("/songs")
public class SongController {

    @Autowired
    final SongService songService;

    @GetMapping
    public String showAllSongs(Model model){
        List<Song> songList = new ArrayList<>();
        if(AutorizedUser.role == Role.ADMIN) {
            songList = songService.findAllSongs();
        }else{
            songList = songService.findAllFreeSongs();
        }
        model.addAttribute("songs", songList);
        model.addAttribute("account_role", AutorizedUser.role.name());
        return "song";
    }

    @PostMapping("/add")
    public String createSong(SongForm songForm){
        songService.createSong(songForm);
        return "redirect:/songs";
    }

    @GetMapping("/update/{song_id}")
    public String showSongInfo(@PathVariable(name = "song_id") Integer songId, Model model){
        try {
            Song song = songService.findSongById(songId);
            model.addAttribute("song", song);
            return "songEdit";
        } catch (SongNotFoundException e) {
            e.printStackTrace();
            return "redirect:/songs";
        }
    }

    @PostMapping("/update")
    public String updateSong(SongForm songForm){
        songService.updateSong(songForm);
        return "redirect:/songs";
    }

    @GetMapping("/take/{song_id}")
    public String takeSong(@PathVariable(name = "song_id") Integer songId){
        songService.fixSongToAccount(songId);
        return "redirect:/songs";
    }

    @GetMapping("/delete/{song_id}")
    public String deleteSong(@PathVariable(name = "song_id") Integer songId){
        songService.deleteSong(songId);
        return "redirect:/songs";
    }

    @GetMapping("/my")
    public String showAccountSongs(Model model){
        List<Song> songList = songService.findAccountSongs();
        model.addAttribute("songs", songList);
        return "songJournal";
    }

    @GetMapping("/handOver/{song_id}")
    public String handOverSong(@PathVariable(name = "song_id") Integer songId){
        songService.handOverSong(songId);
        return "redirect:/songs";
    }
}
package com.iritkov.finalproject.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.iritkov.finalproject.form.AccountForm;
import com.iritkov.finalproject.service.SignUpService;

@Controller
@RequiredArgsConstructor
@RequestMapping("/sign_up")
public class SignUpController {

    @Autowired
    private final SignUpService signUpService;

    @GetMapping
    public String getSignUpPage(){
        return "signUp";
    }

    @PostMapping
    public String signUpUser(AccountForm accountForm){
        signUpService.signUpUser(accountForm);
        return "redirect:/sign_in";
    }
}

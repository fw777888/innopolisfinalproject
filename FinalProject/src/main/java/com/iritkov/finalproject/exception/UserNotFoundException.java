package com.iritkov.finalproject.exception;

import org.springframework.stereotype.Component;

@Component
public class UserNotFoundException extends Exception{

    public UserNotFoundException() {
    }

    public UserNotFoundException(String message) {
        super(message);
    }
}

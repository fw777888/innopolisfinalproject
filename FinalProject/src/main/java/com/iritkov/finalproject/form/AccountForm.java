package com.iritkov.finalproject.form;
import lombok.Data;

@Data
public class AccountForm {
      String name;
      String email;
      String password;

}

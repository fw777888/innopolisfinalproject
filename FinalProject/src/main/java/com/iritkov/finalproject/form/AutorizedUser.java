package com.iritkov.finalproject.form;

import lombok.Data;
import com.iritkov.finalproject.entity.Role;

@Data
public class AutorizedUser {
    public static Integer id;
    public static String name;
    public static Role role;

}

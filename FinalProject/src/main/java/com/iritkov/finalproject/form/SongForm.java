package com.iritkov.finalproject.form;

import lombok.Data;

@Data
public class SongForm {
    String name;
    String description;
    String author;
    Double rating;
    Integer id;
}

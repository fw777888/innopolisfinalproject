package com.iritkov.finalproject.entity;

import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = "account" )
@Builder
@Entity
public class Song {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String name;
    String description;
    String author;
    Double rating;
    @ManyToOne
    @JoinColumn(name = "account_id")
    Account account;
}

package com.iritkov.finalproject.entity;

public enum Role {
    ADMIN, VISITOR
}

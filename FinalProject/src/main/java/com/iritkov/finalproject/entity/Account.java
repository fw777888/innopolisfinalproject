package com.iritkov.finalproject.entity;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = "songs")
@Builder
@Entity
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String name;
    @Column(unique = true)
    String  email;
    String hashPassword;
    @Enumerated(EnumType.STRING)
    Role role;

    @OneToMany(mappedBy = "account")
    List<Song> songList = new ArrayList<>();
}

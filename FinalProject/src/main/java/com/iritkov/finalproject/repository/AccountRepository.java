package com.iritkov.finalproject.repository;

import com.iritkov.finalproject.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<Account, Integer > {

    Optional<Account> findByEmail(String email);
    Account findByName(String name);

}

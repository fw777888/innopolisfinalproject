package com.iritkov.finalproject.repository;

import com.iritkov.finalproject.entity.Account;
import com.iritkov.finalproject.entity.Song;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SongRepository extends JpaRepository<Song, Integer> {
    List<Song> findAllByAccountIsNull();

    List<Song> findAllByAccount(Account account);
}

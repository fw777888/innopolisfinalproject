package com.iritkov.finalproject.service;

import com.iritkov.finalproject.entity.Song;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import com.iritkov.finalproject.entity.Account;
import com.iritkov.finalproject.exception.SongNotFoundException;
import com.iritkov.finalproject.form.SongForm;
import com.iritkov.finalproject.repository.SongRepository;
import com.iritkov.finalproject.security.UserDetailsImpl;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SongService {

    @Autowired
    final SongRepository songRepository;

    public List<Song> findAllSongs() {

        return songRepository.findAll();
    }

    public void createSong(SongForm songForm) {
        Song song = com.iritkov.finalproject.entity.Song.builder()
                .name(songForm.getName())
                .description(songForm.getDescription())
                .author(songForm.getAuthor())
                .rating(songForm.getRating())
                .build();
        songRepository.save(song);
    }

    public List<Song> findAllFreeSongs() {
        return songRepository.findAllByAccountIsNull();
    }

    public void fixSongToAccount(Integer songId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl)authentication.getPrincipal();
        Account user = userDetails.getAccount();
        try {
            Song song = findSongById(songId);
            song.setAccount(user);
            songRepository.save(song);
        } catch (SongNotFoundException e) {
            System.out.println("не удалось найти книгу");
        }

    }

    public void deleteSong(Integer songId){
      try{
          Song song = findSongById(songId);
          if(song.getAccount()==null)
              songRepository.delete(song);
      }catch (SongNotFoundException e) {
          System.out.println("нет такой книги");
      }
    }

    public List<Song> findAccountSongs() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl)authentication.getPrincipal();
        Account account = userDetails.getAccount();
        return songRepository.findAllByAccount(account);
    }

    public Song findSongById(Integer songId) throws SongNotFoundException {
        return songRepository.findById(songId)
                .orElseThrow(()->new SongNotFoundException());
    }

    public void handOverSong(Integer songId) {
        try {
            Song song = findSongById(songId);
            song.setAccount(null);
            songRepository.save(song);
        } catch (SongNotFoundException e) {
            System.out.println("не удалось найти аудиофайл");
        }
    }

    public void updateSong(SongForm songForm) {
        try {
            Song song = findSongById(songForm.getId());
            song.setName(songForm.getName());
            song.setDescription(songForm.getDescription());
            song.setAuthor(songForm.getAuthor());
            song.setRating(songForm.getRating());
            songRepository.save(song);
        } catch (SongNotFoundException e) {
            System.out.println("не удалось найти аудиофайл");
        }
    }
}

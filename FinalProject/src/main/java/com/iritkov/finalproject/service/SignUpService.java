package com.iritkov.finalproject.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.iritkov.finalproject.entity.Account;
import com.iritkov.finalproject.entity.Role;
import com.iritkov.finalproject.form.AccountForm;
import com.iritkov.finalproject.repository.AccountRepository;

@Service
@RequiredArgsConstructor
public class SignUpService {

    @Autowired
    private final AccountRepository accountRepository;
    private final PasswordEncoder passwordEncoder;
    /**
     * Регистрация пользователей
     * @param accountForm
     */
    public void signUpUser(AccountForm accountForm) {

        Account account = Account.builder()
                .name(accountForm.getName())
                .email(accountForm.getEmail())
                .hashPassword(passwordEncoder.encode(accountForm.getPassword()))
                .role(Role.VISITOR)
                .build();
        accountRepository.save(account);
    }
}
